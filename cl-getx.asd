(defsystem "cl-getx"
  :description "This is a naive, persisted, in memory (lazy loading) data store for Common Lisp."
  :version "2020.7.15"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on ()
  :components ((:file "packages")
	       (:file "getx" :depends-on ("packages"))
	       (:file "digx" :depends-on ("getx"))
	       ))

