(in-package :cl-getx)

;;TODO: Test args
(defmacro pick-accessor-expansion (accessor place default-form &rest args)
  "This is used to choose between a function form or a value form of the place. 

default-form can be a function name or \"something\" more complex."
  
  (let ((accessor% (gensym))
	(place% (gensym))
	(default-form% (gensym)))
    
    `(let ((,accessor% ,accessor)
	   (,place% ,place)
	   (,default-form% ,default-form))

       (if (functionp ,accessor%)
	   (apply ,accessor% (list ,place% ,@args))
	   (if (fboundp ,accessor%)
	       (apply ,accessor% (list ,place% ,@args))
	       ,(if (listp default-form)
		    (if (listp (car default-form))			
			default-form%
			`(apply ,default-form% (list ,place% ,accessor% ,@args)))
		    `(apply ,default-form% (list ,place% ,accessor% ,@args))))))))


(defgeneric getx (place accessor &key &allow-other-keys)
  (:documentation "Returns the data stored in a place by name or named-accessor or concrete accessor function. 

Currently places, hash-tables and plists are implemented. Methods for specific types or other lisp types can be implemented easially.

Examples:

> (getx (list :eish 1) :eish)
1

> (getx (list 'eish 1) 'eish)
1

> (getx (list 'eish 1) #'(lambda (place) (getf place 'eish)))
1

(defclass thingy () ((eish :initarg :eish :accessor eish)))
(setf x (make-instance 'thingy 'eish 1))
> (getx x 'eish)
1

**EXTREME** 

Or if you really want uniform thinking and syntax for your data places
This is not a good idea if you are writing a library that others will use
some one might clobber your :eish by accident or you might clobber their :eish

(defclass thingy () ((eish :initarg :eish :accessor :eish)))
(setf x (make-instance 'thingy :eish 1))
> (getx x :eish)
1

"))


(defmethod getx (place accessor &key &allow-other-keys)
  (pick-accessor-expansion accessor place #'getf))

(defmethod getx ((place hash-table) accessor &key default &allow-other-keys)  
  (pick-accessor-expansion accessor place (gethash accessor place default)))

(defmethod getx ((place standard-object) accessor &key  &allow-other-keys)
  (pick-accessor-expansion accessor place #'slot-value))

;;TODO: Test args
(defmacro pick-setf-accessor-expansion (accessor place value default-form &rest args)  
  (let ((accessor% (gensym))
	(place% (gensym))
	(default-form% (gensym))
	(value% (gensym)))
    
    `(let ((,accessor% ,accessor)
	   (,place% ,place)
	   (,default-form% ,default-form)
	   (,value% ,value))
      
       (etypecase ,accessor%
	 (function
	  (apply ,accessor% (list ,place% ,value% ,@args)))    
	 (symbol
	  (if (fboundp ,accessor%)
	      (eval `(setf (,,accessor% ,,place%) ,,value%))
	      ,(if (listp default-form)		   
		  default-form%
		  `(setf (,default-form% ,accessor% ,place% ,@args) ,value%))))))))

(defgeneric (setf getx) (value place accessor &key &allow-other-keys)
  (:documentation "Specialize as you need. plists, hash-tables and standard-object is taken care of."))

(defmethod (setf getx) (value (place list) accessor &key &allow-other-keys)
  "This is for use with (setf getx) of gethash."
  (if (not (functionp accessor))
      (loop for cons = place then (cddr cons)
		    for (key nil . rest) = cons
		    when (eq key accessor)
		    do (setf (cadr cons) value)
		      (return)
		    unless rest
		    do (setf (cddr cons) (list accessor value))
		      (return))
      (pick-setf-accessor-expansion accessor place value
				    (setf (getf place accessor) value))))

(defmethod (setf getx) (value (place hash-table) accessor &key default &allow-other-keys)
  "This is for use with (setf getx) of gethash."
  (pick-setf-accessor-expansion accessor place value
				(setf (gethash accessor place default) value)))

(defmethod (setf getx) (value (place standard-object) accessor &key &allow-other-keys)
  "This is for use with (setf getx) of standard-objects."
  (pick-setf-accessor-expansion accessor place value #'slot-value))

(defgeneric place-exists-p (place accessor)
  (:documentation "Returns t if the place exists. Default implementation is
for plists."))

(defmethod place-exists-p (place accessor)
  (get-properties place (list accessor)))

(defmethod place-exists-p ((place hash-table) key)
  "This is a bit of a waste to do since gethash gives you the value and tells you
if the key is not found, but its here for completeness."
  (multiple-value-bind (value exists-p)
      (gethash key place)
    (values 
     exists-p
     value)))

(defmethod place-exists-p ((place standard-object) slot-name)
  "This is a bit of a waste to do since gethash gives you the value and tells you
if the key is not found, but its here for completeness."
  (slot-exists-p place slot-name))

