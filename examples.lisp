(require 'cl-getx)


(getx (list :eish 1) :eish)

(let ((plist (list :eish 1)))
  (setf (getx plist :eish) 2)
  (getx plist :eish))

(getx (list 'eish 1) 'eish)

(let ((plist (list 'eish 1)))
  (setf (getx plist 'eish) 2)
  (getx plist 'eish))


(getx (list 'eish 1) #'(lambda (place) (getf place 'eish)))


(let ((plist (list 'eish 1)))
  (setf (getx plist #'(lambda (place value)
			(setf (getf place 'eish) value)))
	2)
  (getx plist #'(lambda (place)
		  (getf place 'eish))))


(defclass thingy ()
  ((some-slot :initarg :some-slot :accessor some-slot)))

(setf x (make-instance 'thingy 'eish 1))

(getx x 'eish)

(setf (getx x 'some-slot) 2)

(getx x 'some-slot)


(let ((plist (list :eish (list :huh 1)))) 
    (setf (cl-getx:digx plist :eish :huh) 'huh) 
    plist)

(let ((plist (list :eish (list 'huh 1)))) 
    (setf (cl-getx:digx plist :eish 'huh) 'huh) 
    plist)

(let ((plist (list :eish (list 'huh (list :erm 1))))) 
  (setf (cl-getx:digx plist :eish 'huh #'(lambda (place &optional value)
					   (setf (getf place ':erm) value)))
	'huh) 
  plist)
