(in-package :common-lisp-user)

(defpackage :cl-getx
  (:use :cl)
  (:export

   ;;getx

   :pick-accessor-expansion
   :getx
   :setem
   :pick-setf-accessor-expansion

   ;;digx

   :digx
   
   :place-exists-p
   
  
   ))
